$(document).ready(function(){
    
    $('a').click(function(){
        $('.active-page').toggleClass('active-page');
        $(this).toggleClass('active-page');
    });
    
    var target = $("#videos").offset().top,
    timeout = null;

    $(window).scroll(function () {
        if (!timeout) {
            timeout = setTimeout(function () {
                console.log('scroll');            
                clearTimeout(timeout);
                timeout = null;
                if ($(window).scrollTop() >= $("#photos").offset().top - 200) {
                     $('.active-page').toggleClass('active-page');
                     $('.photos-nav').toggleClass('active-page');
                }
                if ($(window).scrollTop() >= $("#videos").offset().top - 400) {
                     $('.active-page').toggleClass('active-page');
                     $('.videos-nav').toggleClass('active-page');
                }
                if ($(window).scrollTop() >= $("#about").offset().top - 400) {
                     $('.active-page').toggleClass('active-page');
                     $('.about-nav').toggleClass('active-page');
                }
            }, 250);
        }
    });
    
});